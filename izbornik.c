#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "functions.h"

int izbornik(const char* const imeDatoteke) {

	printf("\t\t\t=================================\n");
	printf("\t\t\t\tGLAVNI IZBORNIK\n");
	printf("\t\t\t=================================\n");
	printf("\t\t\tOpcija 1: kreiranje datoteke!\n");
	printf("\t\t\tOpcija 2: dodavanje podataka iz jedne utrke!\n");
	printf("\t\t\tOpcija 3: ucitavanje podataka!\n");
	printf("\t\t\tOpcija 4: ispisivanje podataka jedene utrke!\n");
	printf("\t\t\tOpcija 5: pretraga rezultata jednog konstruktora!\n");
	printf("\t\t\tOpcija 6: pretraga rezultata jednog vozaca!\n");
	printf("\t\t\tOpcija 7: izlaz iz programa!\n\n");
	printf("\t\t\tUpisite broj jedne od opcija:\n");

	int uvijet = 0;
	static PODATAK* poljePodataka = NULL;
	scanf("%d", &uvijet);


	switch (uvijet) {
	case 1:
		system("cls");
		kreiranjeDatoteke(imeDatoteke);
		break;
	case 2:
		system("cls");
		dodajPodatak(imeDatoteke);
		break;
	case 3:
		system("cls");
		if (poljePodataka != NULL) {
			free(poljePodataka);
			poljePodataka = NULL;
		}
		poljePodataka = (PODATAK*)ucitavanjePodataka(imeDatoteke);
		if (poljePodataka == NULL) {
			exit(EXIT_FAILURE);
		}
		break;
	case 4:
		system("cls");
		ispisivanjePodataka(poljePodataka);
		break;
	case 5:
		system("cls");
		pretragaKonstruktora(poljePodataka);
		break;
	case 6:
		system("cls");
		pretragaVozaca(poljePodataka);
		break;
	case 7:
		system("cls");
		uvijet = izlazIzPrograma(poljePodataka);
		break;
	default:
		system("cls");
		uvijet = 0;
	}

	return uvijet;

}
