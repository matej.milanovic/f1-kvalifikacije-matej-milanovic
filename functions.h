#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "dataType.h"

int brojVozacaNaUtrci();
int izbornik(const char* const);
void kreiranjeDatoteke(const char* const);
void dodajPodatak(const char* const);
void* ucitavanjePodataka(const char* const);
int kreiranje(const PODATAK* const);
void kopiranje(PODATAK*, const int);
void sortiranjePodataka(PODATAK, int);
void ispisivanjePodataka(const PODATAK* const);
void pretragaVozaca(const PODATAK* const);
void pretragaKonstruktora(const PODATAK* const);
int izlazIzPrograma(PODATAK*);


#endif
