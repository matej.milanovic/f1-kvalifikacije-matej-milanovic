#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"

static int brojPodataka = 0;

void kreiranjeDatoteke(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "wb");
	if (pF == NULL) {
		perror("Problem prilikom kreiranja datoteke");
		exit(EXIT_FAILURE);
	}
	fwrite(&brojPodataka, sizeof(int), 1, pF);
	fclose(pF);
	printf("Datoteka kreirana!\n");
}

int brojVozacaNaUtrci() {
	int brojVozaca;
	do {
		printf("Unesite broj vozaca na utrci: ");
		scanf("%d", &brojVozaca);
	} while (1 > brojVozaca || brojVozaca > 20);
	return brojVozaca;
}

void dodajPodatak(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "rb+");
	if (pF == NULL) {
		perror("Problem prilikom dodavanja podataka");
		exit(EXIT_FAILURE);
	}
	

	fread(&brojPodataka, sizeof(int), 1, pF);
	printf("Trenutni broj upisanih podataka: %d\n", brojPodataka);
	int brojVozaca = brojVozacaNaUtrci();
	PODATAK temp[20];
	char tempUtrka[50];
	printf("Unesite ime utrke!\n");
	scanf("%49s", tempUtrka);

	for (int i = 0; i < brojVozaca; i++) {
		system("cls");
		printf("%d. VOZAC\n\n", i+1);
		printf("Unesite indentifikacijski broj vozaca!\n");
		scanf("%d", &temp[i].broj);
		printf("Unesite ime vozaca!\n");
		scanf("%29s", temp[i].ime);
		printf("Unesite prezime vozaca!\n");
		scanf("%29s", temp[i].prezime);
		printf("Unesite naziv konstruktora za koji vozi vozac!\n");
		scanf("%29s", temp[i].konstruktor);
		printf("Unesite vrijeme koje je postavio vozac!\n");
		scanf("%f", &temp[i].vrijeme);
		strcpy(temp[i].utrka, tempUtrka);
	}
	fseek(pF, sizeof(PODATAK) * brojPodataka, SEEK_CUR);
	fwrite(&temp, sizeof(PODATAK), brojVozaca, pF);
	rewind(pF);
	brojPodataka += brojVozaca;
	fwrite(&brojPodataka, sizeof(int), 1, pF);
	fclose(pF);
	
}

void* ucitavanjePodataka(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "rb");
	if (pF == NULL) {
		perror("Problem prilikom zauzimanja podataka - datoteka");
		return NULL;
	}
	fread(&brojPodataka, sizeof(int), 1, pF);
	printf("Trenutni broj podataka u datoteci: %d\n", brojPodataka);
	PODATAK* poljePodataka = (PODATAK*)calloc(brojPodataka, sizeof(PODATAK));
	if (poljePodataka == NULL) {
		perror("Problem prilikom zauzimanja memorije");
		return NULL;
	}
	fread(poljePodataka, sizeof(PODATAK), brojPodataka, pF);
	return poljePodataka;
}

/* Testiranje ispravnosti upisa */
/*
void ispisivanjePodataka(const PODATAK* const poljePodataka) {
	if (poljePodataka == NULL) {
		printf("Polje podataka je prazno!\n");
		return;
	}
	for (int i = 0; i < brojPodataka; i++) {
		printf("%d.\t Vozac broj: %d\time: %s\tprezime: %s\tkonstruktor: %s\tvrijeme: %f\tutrka: %s\n",
			i + 1,
			(poljePodataka + i)->broj,
			(poljePodataka + i)->ime,
			(poljePodataka + i)->prezime,
			(poljePodataka + i)->konstruktor,
			(poljePodataka + i)->vrijeme,
			(poljePodataka + i)->utrka);
		
	}
}*/

int kreiranje(const PODATAK* const poljePodataka) {
	int brojac = 0;
	char tempUtrka[50];
	printf("Unesite ime utrke cije rezultate zelite znati!\n");
	scanf("%49s", tempUtrka);

	FILE* fp = NULL;
	fp = fopen("podatcizasortiranje.bin", "wb");
	if (fp == NULL) {
		printf("Ne moze se kreirati datoteka za pretrazvanje rezultata utrke\n");
	}
	else {

		for (int i = 0; i < brojPodataka; i++)
		{
			if (strcmp(tempUtrka, (poljePodataka + i)->utrka) == 0) {
				fwrite((poljePodataka + i), sizeof(PODATAK), 1, fp);
				brojac++;
			}
		}
		fclose(fp);
	}
	return brojac;
}

void kopiranje(PODATAK* temp, const int brojac) {

	FILE* fp = NULL;
	fp = fopen("podatcizasortiranje.bin", "rb");

	if (fp == NULL) {
		printf("Ne mogu se kopirati podatci u polje za svrhu pretrazivanja rezultata.\n");
	}
	else {
		for (int i = 0; i < brojac; i++)
		{
			fread((temp + i), sizeof(PODATAK), 1, fp);
		}
		fclose(fp);
	}
	return;
}

void sortiranjePodataka(PODATAK list[], int s) {
	PODATAK temp;

	for (int i = 0; i < s - 1; i++)
	{
		for (int j = 0; j < (s - 1 - i); j++)
		{
			if (list[j].vrijeme > list[j + 1].vrijeme)
			{
				temp = list[j];
				list[j] = list[j + 1];
				list[j + 1] = temp;
			}
		}
	}
}

void ispisivanjePodataka(const PODATAK* const poljePodataka) {
	if (poljePodataka == NULL) {
		printf("Polje podataka je prazno!\n");
		return;
	}
	int brojac;
	PODATAK temp[20];
	brojac = kreiranje(poljePodataka);
	kopiranje(temp, brojac);
	sortiranjePodataka(temp, brojac);

	for (int i = 0; i < brojac; i++) {
		printf("%d.\t Vozac broj: %d\time: %s\tprezime: %s\tkonstruktor: %s\tvrijeme: %.2f\tutrka: %s\n",
			i + 1,
			temp[i].broj,
			temp[i].ime,
			temp[i].prezime,
			temp[i].konstruktor,
			temp[i].vrijeme,
			temp[i].utrka);
	}
	return;
}

void pretragaKonstruktora(const PODATAK* const poljePodataka) {
	if (poljePodataka == NULL) {
		printf("Polje podataka je prazno!\n");
		return;
	}

	char tempKonstruktor[30];
	printf("Unesite naziv konstukora cije rezultate zelite vidjeti: \n");
	scanf("%29s", tempKonstruktor);

	for (int i = 0; i < brojPodataka; i++) {
		if (strcmp(tempKonstruktor, (poljePodataka + i)->konstruktor) == 0) {
			printf("Vozac broj: %d\time: %s\tprezime: %s\tkonstruktor: %s\tvrijeme: %.2f\tutrka: %s\n",
				(poljePodataka + i)->broj,
				(poljePodataka + i)->ime,
				(poljePodataka + i)->prezime,
				(poljePodataka + i)->konstruktor,
				(poljePodataka + i)->vrijeme,
				(poljePodataka + i)->utrka);
		}

	}
	return;
}

void pretragaVozaca(const PODATAK* const poljePodataka) {
	if (poljePodataka == NULL) {
		printf("Polje podataka je prazno!\n");
		return;
	}

	int tempBroj;
	printf("Unesite indentifikacijsaki broj vozaca cije rezultate zelite vidjeti: \n");
	scanf("%d", &tempBroj);

	for (int i = 0; i < brojPodataka; i++) {
		if (tempBroj == (poljePodataka + i)->broj) {
			printf("Vozac broj: %d\time: %s\tprezime: %s\tkonstruktor: %s\tvrijeme: %.2f\tutrka: %s\n",
				(poljePodataka + i)->broj,
				(poljePodataka + i)->ime,
				(poljePodataka + i)->prezime,
				(poljePodataka + i)->konstruktor,
				(poljePodataka + i)->vrijeme,
				(poljePodataka + i)->utrka);
		}

	}
	return;
}

int izlazIzPrograma(PODATAK* poljePodataka) {
	free(poljePodataka);
	return 0;
}
